/*
 * button.c
 *
 *  Created on: Apr 20, 2021
 *      Author: Mateusz Salamon
 */
#include "main.h"

#include "button.h"

// Button Init
void ButtonInitKey(TButton* Key, GPIO_TypeDef* GpioPort, uint16_t GpioPin, uint32_t	TimerDebounce)
{
	Key->State = IDLE; // Set initial state for the button

	Key->GpioPort = GpioPort; // Remember GPIO Port for the button
	Key->GpioPin = GpioPin; // Remember GPIO Pin for the button

	Key->TimerDebounce = TimerDebounce; // Remember Debounce Time for the button
}
// Time setting functions

// Register callbacks
void ButtonRegisterPressCallback(TButton* Key, void (*Callback)())
{
	Key->ButtonPressed = Callback; // Set new callback for button press
}

// States routine
void ButtinIdleRoutine(TButton* Key)
{
	// Check if button was pressed
	if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(Key->GpioPort, Key->GpioPin))
	{
		// Button was pressed for the first time
		Key->State = DEBOUNCE; // Jump to DEBOUNCE State
		Key->LastTick = HAL_GetTick(); // Remember current tick for Debounce software timer
	}
}

void ButtinDebounceRoutine(TButton* Key)
{
	// Wait for Debounce Timer elapsed
	if((HAL_GetTick() - Key->LastTick) > Key->TimerDebounce)
	{
		// After Debounce Timer elapsed check if button is still pressed
		if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(Key->GpioPort, Key->GpioPin))
		{
			// Still pressed
			Key->State = PRESSED; // Jump to PRESSED state

			if(Key->ButtonPressed != NULL) // Check if callback for pressed button exists
			{
				Key->ButtonPressed(); // If exists - do the callback function
			}
		}
		else
		{
			// If button was released durong debounce time
			Key->State = IDLE; // Go back do IDLE state
		}
	}
}

void ButtinPressedRoutine(TButton* Key)
{
	// Check if button was released
	if(GPIO_PIN_SET == HAL_GPIO_ReadPin(Key->GpioPort, Key->GpioPin))
	{
		// If released - go back to IDLE state
		Key->State = IDLE;
	}
	// Otherwise - do nothing
}

// State Machine
void ButtonTask(TButton* Key)
{
	switch(Key->State)
	{
		case IDLE:
			// do IDLE
			ButtinIdleRoutine(Key);
			break;

		case DEBOUNCE:
			// do DEBOUNCE
			ButtinDebounceRoutine(Key);
			break;

		case PRESSED:
			// do PRESSED
			ButtinPressedRoutine(Key);
			break;

		default:
			break;
	}
}
