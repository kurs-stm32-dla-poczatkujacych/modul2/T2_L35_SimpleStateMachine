/*
 * button.h
 *
 *  Created on: Apr 20, 2021
 *      Author: Mateusz Salamon
 */

#ifndef INC_BUTTON_H_
#define INC_BUTTON_H_

// States for state machine
typedef enum
{
	IDLE = 0,
	DEBOUNCE,
	PRESSED
} BUTTON_STATE;

// Struct for button
typedef struct
{
	BUTTON_STATE 	State; // Button current state

	GPIO_TypeDef* 	GpioPort; // GPIO Port for a button
	uint16_t		GpioPin; // GPIO Pin for a button

	uint32_t		LastTick; // Last remembered time before steps
	uint32_t		TimerDebounce; // Fixed, settable time for debounce timer

	void(*ButtonPressed)(void); // A callback for buttos pressed
} TButton; // Button_t

// Public functions
void ButtonTask(TButton* Key);

void ButtonRegisterPressCallback(TButton* Key, void (*Callback)());

void ButtonInitKey(TButton* Key, GPIO_TypeDef* GpioPort, uint16_t GpioPin, uint32_t	TimerDebounce);

#endif /* INC_BUTTON_H_ */
